#!/bin/bash

file=$1
file2Store="${1%.*}.rls"
originalRule=""
rulewerkRule=""

while read p;
do
	part=$(echo "$p" | tr -d '\r')
	originalRule+=$part
	if [[ $part =~ "." ]]; then
		head=$(echo "${originalRule#*->}" | tr -d '.')
		body=${originalRule%->*}

		IFS=\, read -a fields <<<"$head"
		for x in "${fields[@]}" ;
		do
			variable=$(echo "$x" | sed 's/.*(//g' | sed 's/ //g' | sed 's/).*//g')
			variableName=$(echo "$variable" | tr -d '?')

			if [[ ! $body =~ "$variable," ]] && [[ ! $body =~ "$variable)" ]];
			then
				head=$(echo "$head" | sed "s/$variable,/!$variableName,/g")
				head=$(echo "$head" | sed "s/$variable)/!$variableName)/g")
			fi
		done

		echo "${head}:- ${body}." >> "$file2Store"

		originalRule=""
	fi
done < "$file"