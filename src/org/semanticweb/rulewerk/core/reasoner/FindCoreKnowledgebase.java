package org.semanticweb.rulewerk.core.reasoner;

import org.semanticweb.rulewerk.core.model.api.PositiveLiteral;
import org.semanticweb.rulewerk.core.model.api.Predicate;

import java.util.Map;
import java.util.Set;

public class FindCoreKnowledgebase extends KnowledgeBase
{
    public Map<Predicate, Set<PositiveLiteral>> getFactsPerPredicate() {
        return super.getFactsByPredicate();
    }
}
