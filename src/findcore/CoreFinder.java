package findcore;

import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.BiconnectivityInspector;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.interfaces.StrongConnectivityAlgorithm;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultUndirectedGraph;
import org.semanticweb.rulewerk.core.model.api.*;
import org.semanticweb.rulewerk.core.model.implementation.*;
import org.semanticweb.rulewerk.core.reasoner.FindCoreKnowledgebase;
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase;
import org.semanticweb.rulewerk.core.reasoner.Reasoner;
import org.semanticweb.rulewerk.parser.ParsingException;
import org.semanticweb.rulewerk.parser.RuleParser;
import org.semanticweb.rulewerk.reasoner.vlog.VLogReasoner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class CoreFinder
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CoreFinder.class);

    private final FindCoreKnowledgebase knowledgeBase_T;
    private final FindCoreKnowledgebase knowledgeBase_T_Sigma;
    private final FindCoreKnowledgebase knowledgeBase_Closure;

    private FindCoreKnowledgebase core;

    private final Set<NamedNull> namedNulls_U = new HashSet<>();
    private final HashSet<NamedNull> namedNullsToCheck = new HashSet<>();

    private final HashSet<NamedNull> dirtyNamedNulls = new HashSet<>();
    private DefaultUndirectedGraph<Term, DefaultEdge> dirtyRelatednessGraph = new DefaultUndirectedGraph<>(DefaultEdge.class);
    private int oldNumberOfDirtyOnes = 0;

    private DefaultUndirectedGraph<Term, DefaultEdge> relatednessGraph = new DefaultUndirectedGraph<>(DefaultEdge.class);
    private DefaultUndirectedGraph<NamedNull, DefaultEdge> gaifmannGraph = new DefaultUndirectedGraph<>(DefaultEdge.class);
    private final Set<Term> terms = new HashSet<>();

    private int numberOfReductions = 0;
    private int numberOfReducedNulls = 0;

    private static long totalVLogTime = 0;
    private static long totalGraphPreparationTime = 0;

    CoreFinder(FindCoreKnowledgebase _knowledgeBase_T, FindCoreKnowledgebase _knowledgeBase_T_Sigma, FindCoreKnowledgebase _knowledgeBase_Closure)
    {
        knowledgeBase_T = _knowledgeBase_T;
        knowledgeBase_T_Sigma = _knowledgeBase_T_Sigma;
        knowledgeBase_Closure = _knowledgeBase_Closure;

        for (Fact fact : knowledgeBase_T_Sigma.getFacts())
        {
            namedNulls_U.addAll(fact.getNamedNulls().collect(Collectors.toList()));
            terms.addAll(fact.getArguments());
        }

        namedNullsToCheck.addAll(namedNulls_U);
        LOGGER.debug("NamedNulls: " + namedNulls_U.size());
        LOGGER.debug("all Terms: " + terms.size());
    }

    public static CoreFinder findCore(String pathForModel_T, String pathForModel_T_Sigma, boolean randomY)
    {
        long start = System.nanoTime();
        FindCoreKnowledgebase findCoreKnowledgeBase_T = new FindCoreKnowledgebase();

        FindCoreKnowledgebase findCoreKnowledgeBase_T_Sigma = new FindCoreKnowledgebase();
        FindCoreKnowledgebase findCoreKnowledgeBase_Closure = new FindCoreKnowledgebase();

        try
        {
            findCoreKnowledgeBase_T.addStatements(RuleParser.parse(new FileInputStream(pathForModel_T)).getFacts());
            findCoreKnowledgeBase_T_Sigma.addStatements(findCoreKnowledgeBase_T.getFacts());

            KnowledgeBase knowledgeBase = RuleParser.parse(new FileInputStream(pathForModel_T_Sigma));
            for (Fact fact : knowledgeBase.getFacts()) {
                if (fact.getPredicate().getName().equals("Closure"))
                    findCoreKnowledgeBase_Closure.addStatements(fact);
                else {
                    findCoreKnowledgeBase_T_Sigma.addStatements(fact);
                }
            }

            CoreFinder coreFinder = new CoreFinder(findCoreKnowledgeBase_T, findCoreKnowledgeBase_T_Sigma, findCoreKnowledgeBase_Closure);
            if (randomY)
                coreFinder.calcCoreWithRandom_Y();
            else
                coreFinder.calcCore();

            long end = System.nanoTime();
            LOGGER.debug("total time: " + (end - start)/1000000 + " ms");
            LOGGER.debug("total VLog time: " + (totalVLogTime)/1000000 + " ms");
            LOGGER.debug("total graph calculating time: " + (totalGraphPreparationTime)/1000000 + " ms");
            LOGGER.debug("total number of reductions: " + coreFinder.numberOfReductions);
            LOGGER.debug("total number of reduced nulls: " + coreFinder.numberOfReducedNulls);
            LOGGER.debug("total number of reduced facts: " + (coreFinder.getT_Sigma().getFacts().size() - coreFinder.getCore().getFacts().size()));

            return coreFinder;
        }
        catch (Exception e)
        {
            LOGGER.debug(e.getMessage());
            return null;
        }
    }

    public static CoreFinder findCore(KnowledgeBase knowledgeBase, boolean randomY)
    {
        long start = System.nanoTime();
        FindCoreKnowledgebase findCoreKnowledgeBase_T = new FindCoreKnowledgebase();
        findCoreKnowledgeBase_T.addStatements(knowledgeBase.getFacts());

        FindCoreKnowledgebase findCoreKnowledgeBase_T_Sigma = new FindCoreKnowledgebase();
        FindCoreKnowledgebase findCoreKnowledgeBase_Closure = new FindCoreKnowledgebase();


        FindCoreKnowledgebase findCoreKnowledgeBaseForVlog = new FindCoreKnowledgebase();
        findCoreKnowledgeBaseForVlog.addStatements(knowledgeBase.getFacts());
        // Add closure facts for constants and variables existing in T
        for (Fact fact : knowledgeBase.getFacts())
        {
            for (Term term : fact.getArguments())
            {
                ArrayList<Term> arguments = new ArrayList<>();
                arguments.add(term);
                arguments.add(term);
                findCoreKnowledgeBaseForVlog.addStatement(new FactImpl(new PredicateImpl("Closure", 2), arguments));
            }
        }

        for (Rule rule : knowledgeBase.getRules())
        {
            if (rule.getExistentialVariables().findFirst().isPresent())
            {
                ArrayList<PositiveLiteral> newHeadLiterals = new ArrayList<>(rule.getHead().getLiterals());
                for (ExistentialVariable existentialVariable : rule.getExistentialVariables().collect(Collectors.toList()))
                    for (Variable variable : rule.getVariables().collect(Collectors.toList()))
                        newHeadLiterals.add(new PositiveLiteralImpl(new PredicateImpl("Closure", 2), List.of(existentialVariable, variable)));

                findCoreKnowledgeBaseForVlog.addStatement(new RuleImpl(new ConjunctionImpl<>(newHeadLiterals), rule.getBody()));
            }
            else
                findCoreKnowledgeBaseForVlog.addStatement(rule);
        }

        // add rule Closure(?x, ?z) :- Closure(?x, ?y), Closure(?y, ?z).
        UniversalVariableImpl x = new UniversalVariableImpl("x");
        UniversalVariableImpl y = new UniversalVariableImpl("y");
        UniversalVariableImpl z = new UniversalVariableImpl("z");

        PositiveLiteralImpl head = new PositiveLiteralImpl(new PredicateImpl("Closure", 2), List.of(x, z));
        PositiveLiteralImpl bodyPart1 = new PositiveLiteralImpl(new PredicateImpl("Closure", 2), List.of(x, y));
        PositiveLiteralImpl bodyPart2 = new PositiveLiteralImpl(new PredicateImpl("Closure", 2), List.of(y, z));

        findCoreKnowledgeBaseForVlog.addStatement(new RuleImpl(new ConjunctionImpl<>(List.of(head)), new ConjunctionImpl<>(List.of(bodyPart1, bodyPart2))));

        try
        {
            LOGGER.debug("Start reasoning ...");
            long vLogStart = System.nanoTime();
            Reasoner reasoner = new VLogReasoner(findCoreKnowledgeBaseForVlog);
            reasoner.reason();

            KnowledgeBase knowledgeBaseInferences = new KnowledgeBase();
            String inferences = reasoner.getInferences().collect(Collectors.toList()).toString();
            reasoner.close();
            long vLogEnd = System.nanoTime();
            long totalVLogForT_Sigma_Time = vLogEnd - vLogStart;
            RuleParser.parseInto(knowledgeBaseInferences, inferences.substring(1, inferences.length()-1).replaceAll("\\.,","\\.\n"));

            for (Fact fact : knowledgeBaseInferences.getFacts())
            {
                if (fact.getPredicate().getName().equals("Closure"))
                    findCoreKnowledgeBase_Closure.addStatements(fact);
                else
                {
                    findCoreKnowledgeBase_T_Sigma.addStatements(fact);
                }
            }

            LOGGER.debug("Creating CoreFinder ...");
            CoreFinder coreFinder = new CoreFinder(findCoreKnowledgeBase_T, findCoreKnowledgeBase_T_Sigma, findCoreKnowledgeBase_Closure);
            LOGGER.debug("Finding core ...");
            if (randomY)
                coreFinder.calcCoreWithRandom_Y();
            else
                coreFinder.calcCore();

            long end = System.nanoTime();
            LOGGER.debug("total time: " + (end - start)/1000000 + " ms");
            LOGGER.debug("T_Sigma VLog time with preparation: " + (vLogEnd - start)/1000000 + " ms");
            LOGGER.debug("T_Sigma VLog time: " + (totalVLogForT_Sigma_Time)/1000000 + " ms");
            LOGGER.debug("rest VLog time: " + (totalVLogTime)/1000000 + " ms");
            LOGGER.debug("total graph calculating time: " + (totalGraphPreparationTime)/1000000 + " ms");
            LOGGER.debug("total number of reductions: " + coreFinder.numberOfReductions);
            LOGGER.debug("total number of reduced nulls: " + coreFinder.numberOfReducedNulls);
            LOGGER.debug("total number of reduced facts: " + (coreFinder.getT_Sigma().getFacts().size() - coreFinder.getCore().getFacts().size()));

            return coreFinder;
        }
        catch (Exception e)
        {
            LOGGER.debug(e.getMessage());
            return null;
        }
    }

    /*
        While Nulls not empty:
            1. Pick x from Nulls, y from Terms
            2. Calc T_XY
            3. Check for homomorphism
                3.1. If yes calc reduction
    */
    public void calcCoreWithRandom_Y()
    {
        FindCoreKnowledgebase knowledgeBase_U = new FindCoreKnowledgebase();
        knowledgeBase_U.addStatements(knowledgeBase_T_Sigma.getFacts());

        LOGGER.debug("calculating gaifmann graph ...");
        long graphsStart = System.nanoTime();
        gaifmannGraph = calcGaifmanGraph(knowledgeBase_U);
        long graphsEnd = System.nanoTime();
        totalGraphPreparationTime = graphsEnd - graphsStart;
        LOGGER.debug("finished graph ...");

        HashSet<Term> alreadyChecked = new HashSet<>();
        NamedNull x = null;

        while (!namedNullsToCheck.isEmpty())
        {
            if (alreadyChecked.isEmpty())
            {
                x = namedNullsToCheck.stream().findAny().orElseThrow();
                alreadyChecked.add(x);
            }

            if (alreadyChecked.size() < terms.size())
            {
                Term y = terms.stream().filter(term -> !alreadyChecked.contains(term)).findAny().orElseThrow();
                if (x != y)
                {
                    try
                    {
                        FindCoreKnowledgebase knowledgeBase_T_XY = calcT_XY(knowledgeBase_U, x, y);
                        DefaultDirectedGraph<Term, DefaultEdge> endomorphismGraph = calcEndomorphism(knowledgeBase_T_XY, knowledgeBase_U, x, y);

                        if (endomorphismGraph != null)
                        {
                            knowledgeBase_U = reduceKnowledgeBase(knowledgeBase_U, getImages(endomorphismGraph));

                            dirtyNamedNulls.remove(x);
                            alreadyChecked.clear();
                        } else
                            alreadyChecked.add(y);
                    } catch (Exception e)
                    {
                        LOGGER.debug(e.getMessage());
                        LOGGER.debug(Arrays.toString(e.getStackTrace()));
                    }
                }
            }
            else
            {
                namedNullsToCheck.remove(x);
                alreadyChecked.clear();
            }

            if (namedNullsToCheck.isEmpty() && oldNumberOfDirtyOnes != dirtyNamedNulls.size())
            {
                LOGGER.debug("Starting with dirty ones again ...");
                oldNumberOfDirtyOnes = dirtyNamedNulls.size();
                namedNullsToCheck.addAll(dirtyNamedNulls);
                dirtyNamedNulls.clear();
            }
        }

        core =  knowledgeBase_U;
    }

    protected void calcCore()
    {
        FindCoreKnowledgebase knowledgeBase_U = new FindCoreKnowledgebase();
        knowledgeBase_U.addStatements(knowledgeBase_T_Sigma.getFacts());

        LOGGER.debug("calculating graphs ...");
        long graphsStart = System.nanoTime();
        relatednessGraph = calcRelatednessGraph(knowledgeBase_U);
        gaifmannGraph = calcGaifmanGraph(knowledgeBase_U);
        long graphsEnd = System.nanoTime();
        totalGraphPreparationTime = graphsEnd - graphsStart;
        LOGGER.debug("finished graphs ...");
        LOGGER.debug("relatedness graphs has " + relatednessGraph.edgeSet().size() + " edges.");

        while (!namedNullsToCheck.isEmpty())
        {
            NamedNull x = namedNullsToCheck.stream().findAny().orElseThrow();
            if (relatednessGraph.outgoingEdgesOf(x) == null || relatednessGraph.outgoingEdgesOf(x).isEmpty())
            {
                namedNullsToCheck.remove(x);
                relatednessGraph.removeVertex(x);
            }
            else
            {
                DefaultEdge edge = relatednessGraph.outgoingEdgesOf(x).stream().findAny().orElseThrow();
                Term y = relatednessGraph.getEdgeTarget(edge);

                if (y.getName().equals(x.getName()))
                    y = relatednessGraph.getEdgeSource(edge);

                try
                {
                    FindCoreKnowledgebase knowledgeBase_T_XY = calcT_XY(knowledgeBase_U, x, y);
                    DefaultDirectedGraph<Term, DefaultEdge> endomorphismGraph = calcEndomorphism(knowledgeBase_T_XY, knowledgeBase_U, x, y);

                    if (endomorphismGraph == null)
                    {
                        relatednessGraph.removeEdge(x, y);
                        if (dirtyNamedNulls.contains(x))
                        {
                            dirtyRelatednessGraph.addVertex(x);
                            dirtyRelatednessGraph.addVertex(y);
                            dirtyRelatednessGraph.addEdge(x, y);
                        }
                    }
                    else
                    {
                        knowledgeBase_U = reduceKnowledgeBase(knowledgeBase_U, getImages(endomorphismGraph));

                        namedNullsToCheck.remove(x);
                    }
                } catch (Exception e)
                {
                    LOGGER.debug(e.getMessage());
                    LOGGER.debug(Arrays.toString(e.getStackTrace()));
                }
            }

            if (namedNullsToCheck.isEmpty() && oldNumberOfDirtyOnes != dirtyRelatednessGraph.edgeSet().size())
            {
                LOGGER.debug("Starting with dirty ones again ...");
                namedNullsToCheck.addAll(dirtyNamedNulls);
                dirtyNamedNulls.clear();

                oldNumberOfDirtyOnes = dirtyRelatednessGraph.edgeSet().size();
                relatednessGraph = (DefaultUndirectedGraph<Term, DefaultEdge>) dirtyRelatednessGraph.clone();
                dirtyRelatednessGraph = new DefaultUndirectedGraph<>(DefaultEdge.class);
            }
        }

        core = knowledgeBase_U;
    }

    protected FindCoreKnowledgebase calcT_XY(FindCoreKnowledgebase knowledgeBase_U, NamedNull x, Term y)
    {
        FindCoreKnowledgebase knowledgeBase_T_xy = new FindCoreKnowledgebase();
        knowledgeBase_T_xy.addStatements(knowledgeBase_T.getFacts());

        Set<Term> e_XY = new HashSet<>();
        for (Fact f : knowledgeBase_Closure.getFacts().stream().filter((fact) -> fact.getArguments().get(0).equals(x)).collect(Collectors.toList()))
            e_XY.addAll(f.getArguments());

        for (Fact f : knowledgeBase_Closure.getFacts().stream().filter((fact) -> fact.getArguments().get(0).equals(y)).collect(Collectors.toList()))
            e_XY.addAll(f.getArguments());

        knowledgeBase_T_xy.addStatements(knowledgeBase_U.getFacts().stream().filter((fact) -> e_XY.containsAll(fact.getArguments())).collect(Collectors.toList()));

        return knowledgeBase_T_xy;
    }

    protected DefaultDirectedGraph<Term, DefaultEdge> calcEndomorphism(FindCoreKnowledgebase knowledgeBase_T_XY, FindCoreKnowledgebase knowledgeBase_U, NamedNull x, Term y) throws ParsingException, IOException
    {
        DefaultDirectedGraph<Term, DefaultEdge> baseGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
        baseGraph.addVertex(x);
        baseGraph.addVertex(y);
        baseGraph.addEdge(x, y);

        String body = knowledgeBase_T_XY.getFacts().toString().replaceAll("\\[", "").replaceAll("]", "").replaceAll("\\.,", ",").replaceAll("_:", "?a");
        if (y.getType().equals(TermType.NAMED_NULL))
            body = body.replaceAll(x.getName(), y.getName());
        else
            body = body.replaceAll("\\?a" + x.getName(), y.getName());

        Set<NamedNull> namedNulls_T_XY = new HashSet<>();
        for (Fact fact : knowledgeBase_T_XY.getFacts())
            namedNulls_T_XY.addAll(fact.getNamedNulls().collect(Collectors.toList()));
        namedNulls_T_XY.remove(x);

        DefaultDirectedGraph<Term, DefaultEdge> bestGraph;
        if (!namedNulls_T_XY.isEmpty())  // empty can happen, when y is a term and x the only variable in T_XY
        {
            StringBuilder head = new StringBuilder("query(");
            for (NamedNull namedNull : namedNulls_T_XY)
                head.append("?a").append(namedNull.getName()).append(",");
            head.replace(head.length() - 1, head.length(), ")");

            List<Fact> inferences = queryRuleWithKnowledgeBase(head + " :- " + body, knowledgeBase_U, x);
            if (inferences.isEmpty())
                return null;

            List<DefaultDirectedGraph<Term, DefaultEdge>> graphs = getGraphList(inferences, baseGraph, namedNulls_T_XY);
            bestGraph = getBestGraph(graphs, x, y);

            if (bestGraph == null)
                return  null;
        }
        else
        {
            List<Fact> inferences = queryRuleWithKnowledgeBase("query(!X)" + " :- " + body, knowledgeBase_U, x);
            if (inferences.isEmpty())
                return null;

            return baseGraph;
        }

        Map<NamedNull, Term> image_T_XY_Map = getImages(bestGraph);
        //Set<NamedNull> namedNullsOutSideOf_T_XY = namedNulls_U.stream().filter(namedNull -> !namedNulls_T_XY.contains(namedNull)).collect(Collectors.toSet());
        Set<NamedNull> namedNullsOutSideOf_T_XY = getImportantNamedNullsOutsideOfT_XY(image_T_XY_Map);
        namedNullsOutSideOf_T_XY.remove(x);

        if (!namedNullsOutSideOf_T_XY.isEmpty())
        {
            Set<Fact> factsOfSecondBody = new HashSet<>();
            for (NamedNull namedNull : namedNullsOutSideOf_T_XY)
                factsOfSecondBody.addAll(knowledgeBase_U.getFacts().stream().filter(fact -> fact.getArguments().contains(namedNull)).collect(Collectors.toList()));

            StringBuilder secondBodyBuilder = new StringBuilder();
            for (Fact fact : factsOfSecondBody) {
                String stringOfFact = fact.toString();
                if (fact.getNamedNulls().anyMatch(namedNull -> namedNulls_T_XY.contains(namedNull) || namedNullsOutSideOf_T_XY.contains(namedNull)))
                    for (NamedNull namedNull : namedNulls_U)
                    {
                        if (image_T_XY_Map.containsKey(namedNull))
                            if (image_T_XY_Map.get(namedNull).getType().equals(TermType.NAMED_NULL))
                                stringOfFact = stringOfFact.replaceAll("_:" + namedNull.getName(), image_T_XY_Map.get(namedNull).toString());
                            else
                                stringOfFact = stringOfFact.replaceAll("_:" + namedNull.getName(), image_T_XY_Map.get(namedNull).toString());
                    }

                secondBodyBuilder.append(stringOfFact.replaceAll("_:", "?a").replaceAll(" \\.", ", "));
            }
            secondBodyBuilder.replace(secondBodyBuilder.length() - 2, secondBodyBuilder.length(), " .");

            StringBuilder secondHeadBuilder = new StringBuilder("query(");
            for (NamedNull namedNull : namedNullsOutSideOf_T_XY)
                secondHeadBuilder.append("?a").append(namedNull.getName()).append(",");
            secondHeadBuilder.replace(secondHeadBuilder.length() - 1, secondHeadBuilder.length(), ")");

            List<Fact> secondInferences = queryRuleWithKnowledgeBase(secondHeadBuilder + " :- " + secondBodyBuilder, knowledgeBase_U, x);
            if (secondInferences.isEmpty())
                return null;
            List<DefaultDirectedGraph<Term, DefaultEdge>> secondGraphList = getGraphList(secondInferences, bestGraph, namedNullsOutSideOf_T_XY);

            bestGraph = getBestGraph(secondGraphList, x, y);
        }

        return bestGraph;
    }

    private Set<NamedNull> getImportantNamedNullsOutsideOfT_XY(Map<NamedNull, Term> map)
    {
        Set<NamedNull> importantNamedNullsOutsideOfT_XY = new HashSet<>();
        List<NamedNull> newlyAddedNamedNulls = new ArrayList<>();

        for (NamedNull namedNull : map.keySet())
            if (map.get(namedNull) != namedNull)
            {
                for (DefaultEdge edge : gaifmannGraph.outgoingEdgesOf(namedNull))
                    if (gaifmannGraph.getEdgeTarget(edge).getType().equals(TermType.NAMED_NULL) && !map.containsKey(gaifmannGraph.getEdgeTarget(edge)))
                    {
                        importantNamedNullsOutsideOfT_XY.add(gaifmannGraph.getEdgeTarget(edge));
                        newlyAddedNamedNulls.add(gaifmannGraph.getEdgeTarget(edge));
                    }
                for (DefaultEdge edge : gaifmannGraph.incomingEdgesOf(namedNull))
                    if (gaifmannGraph.getEdgeSource(edge).getType().equals(TermType.NAMED_NULL) && !map.containsKey(gaifmannGraph.getEdgeSource(edge)))
                    {
                        importantNamedNullsOutsideOfT_XY.add(gaifmannGraph.getEdgeSource(edge));
                        newlyAddedNamedNulls.add(gaifmannGraph.getEdgeTarget(edge));
                    }
            }

        while (!newlyAddedNamedNulls.isEmpty())
        {
            NamedNull namedNull = newlyAddedNamedNulls.get(0);
            for (DefaultEdge edge : gaifmannGraph.outgoingEdgesOf(namedNull))
                if (gaifmannGraph.getEdgeTarget(edge).getType().equals(TermType.NAMED_NULL) && !map.containsKey(gaifmannGraph.getEdgeTarget(edge)) && !importantNamedNullsOutsideOfT_XY.contains(gaifmannGraph.getEdgeTarget(edge)))
                {
                    importantNamedNullsOutsideOfT_XY.add((NamedNull) gaifmannGraph.getEdgeTarget(edge));
                    newlyAddedNamedNulls.add((NamedNull) gaifmannGraph.getEdgeTarget(edge));
                }
            for (DefaultEdge edge : gaifmannGraph.incomingEdgesOf(namedNull))
                if (gaifmannGraph.getEdgeSource(edge).getType().equals(TermType.NAMED_NULL) && !map.containsKey(gaifmannGraph.getEdgeSource(edge)) && !importantNamedNullsOutsideOfT_XY.contains(gaifmannGraph.getEdgeSource(edge)))
                {
                    importantNamedNullsOutsideOfT_XY.add((NamedNull) gaifmannGraph.getEdgeSource(edge));
                    newlyAddedNamedNulls.add((NamedNull) gaifmannGraph.getEdgeTarget(edge));
                }

            newlyAddedNamedNulls.remove(namedNull);
        }

        return importantNamedNullsOutsideOfT_XY;
    }

    protected List<Fact> queryRuleWithKnowledgeBase(String ruleString, FindCoreKnowledgebase knowledgeBase_U, NamedNull namedNull) throws ParsingException, IOException
    {
        long vLogStart = System.nanoTime();
        Rule rule = RuleParser.parseRule(ruleString);

        FindCoreKnowledgebase knowledgeBase = new FindCoreKnowledgebase();
        knowledgeBase.addStatements(knowledgeBase_U.getFacts());
        knowledgeBase.addStatement(rule);

        if(rule.getVariables().count() >= 256)
        {
            LOGGER.error("CRITICAL: Variablen anzahl: " + rule.getVariables().count());
            dirtyNamedNulls.add(namedNull);

            return new ArrayList<>();
        }
        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        List<Fact> queryResults = reasoner.getInferences().filter(fact -> fact.getPredicate().getName().equalsIgnoreCase("query")).collect(Collectors.toList());
        reasoner.close();

        long vLogEnd = System.nanoTime();
        totalVLogTime += vLogEnd - vLogStart;

        return queryResults;
    }

    protected List<DefaultDirectedGraph<Term, DefaultEdge>> getGraphList(List<Fact> inferences, DefaultDirectedGraph<Term, DefaultEdge> baseGraph, Set<NamedNull> namedNulls)
    {
        List<DefaultDirectedGraph<Term, DefaultEdge>> graphs = new ArrayList<>();
        for (Fact fact : inferences)
        {
            DefaultDirectedGraph<Term, DefaultEdge> graph = (DefaultDirectedGraph<Term, DefaultEdge>) baseGraph.clone();
            int i = 0;
            for (NamedNull namedNull : namedNulls)
            {
                Term term = fact.getArguments().get(i++);

                Term termAsNamedNull = null;
                for (Term rulewerkTermToVLogTerm : terms)
                    if (rulewerkTermToVLogTerm.getName().equalsIgnoreCase(term.getName().replaceAll(".*/B-", "")))
                    {
                        termAsNamedNull = rulewerkTermToVLogTerm;
                        break;
                    }

                graph.addVertex(namedNull);
                graph.addVertex(termAsNamedNull);
                graph.addEdge(namedNull, termAsNamedNull);
            }
            graphs.add(graph);
        }

        return graphs;
    }

    protected DefaultDirectedGraph<Term, DefaultEdge> getBestGraph(List<DefaultDirectedGraph<Term, DefaultEdge>> graphs, NamedNull x, Term y)
    {
        Map<Integer, List<DefaultDirectedGraph<Term, DefaultEdge>>> map = new HashMap<>();
        for (DefaultDirectedGraph<Term, DefaultEdge> graph : graphs)
        {
            StrongConnectivityAlgorithm<Term, DefaultEdge> strongConnectivityInspector = new KosarajuStrongConnectivityInspector<>(graph);
            if (strongConnectivityInspector.stronglyConnectedSets().stream().noneMatch(set -> set.size() > 1))
            {
                BiconnectivityInspector<Term, DefaultEdge> biconnectivityInspector = new BiconnectivityInspector<>(graph);
                int graphValue = biconnectivityInspector.getConnectedComponents().size();

                if (map.containsKey(graphValue))
                    map.get(graphValue).add(graph);
                else
                {
                    List<DefaultDirectedGraph<Term, DefaultEdge>> set = new ArrayList<>();
                    set.add(graph);
                    map.put(graphValue, set);
                }
            }
        }

        if (map.isEmpty())
        {
            for (DefaultDirectedGraph<Term, DefaultEdge> graph : graphs)
            {
                graph.removeEdge(x, y);
                StrongConnectivityAlgorithm<Term, DefaultEdge> strongConnectivityInspector = new KosarajuStrongConnectivityInspector<>(graph);
                if (strongConnectivityInspector.stronglyConnectedSets().stream().noneMatch(set -> set.size() > 1))
                {
                    BiconnectivityInspector<Term, DefaultEdge> biconnectivityInspector = new BiconnectivityInspector<>(graph);
                    int graphValue = biconnectivityInspector.getConnectedComponents().size();

                    if (map.containsKey(graphValue))
                        map.get(graphValue).add(graph);
                    else
                    {
                        List<DefaultDirectedGraph<Term, DefaultEdge>> set = new ArrayList<>();
                        set.add(graph);
                        map.put(graphValue, set);
                    }
                }
            }
        }

        return map.get(Collections.min(map.keySet())).get(0);
    }

    protected Map<NamedNull, Term> getImages(DefaultDirectedGraph<Term, DefaultEdge> graph)
    {
        HashMap<NamedNull, Term> imageMap = new HashMap<>();

        BiconnectivityInspector<Term, DefaultEdge> biconnectivityInspector = new BiconnectivityInspector<>(graph);
        for (Graph<Term, DefaultEdge> connectedComponent : biconnectivityInspector.getConnectedComponents())
        {
            Term image = connectedComponent.vertexSet().iterator().next();
            while (graph.outgoingEdgesOf(image).size() > 0 && !graph.containsEdge(image, image))
                image = graph.getEdgeTarget(graph.outgoingEdgesOf(image).iterator().next());

            for (Term term : connectedComponent.vertexSet())
                if (term.getType().equals(TermType.NAMED_NULL) && term != image)
                    imageMap.put((NamedNull) term, image);
        }

        return imageMap;
    }

    protected FindCoreKnowledgebase reduceKnowledgeBase(FindCoreKnowledgebase knowledgeBase, Map<NamedNull, Term> imageMap)
    {
        LOGGER.debug("Reduce knowledgeBase ...");
        numberOfReductions++;

        FindCoreKnowledgebase reducedKnowledgeBase = new FindCoreKnowledgebase();
        for (Fact fact : knowledgeBase.getFacts())
        {
            ArrayList<Term> newArguments = new ArrayList<>();
            for (Term term : fact.getArguments())
            {
                if (term.getType().equals(TermType.NAMED_NULL))
                {
                    if (imageMap.containsKey((NamedNull) term))
                    {
                        newArguments.add(imageMap.get(term));

                        namedNulls_U.remove(term);
                        namedNullsToCheck.remove(term);
                        relatednessGraph.removeVertex(term);
                        terms.remove(term);
                        numberOfReducedNulls++;
                    }
                    else
                        newArguments.add(term);
                }
                else
                    newArguments.add(term);
            }
            reducedKnowledgeBase.addStatement(new FactImpl(fact.getPredicate(), newArguments));
        }

        return reducedKnowledgeBase;
    }

    protected DefaultUndirectedGraph<NamedNull, DefaultEdge> calcGaifmanGraph(FindCoreKnowledgebase knowledgeBase)
    {
        DefaultUndirectedGraph<NamedNull, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);

        for (Fact fact : knowledgeBase.getFacts())
        {
            List<NamedNull> namedNullsOfFact = fact.getNamedNulls().collect(Collectors.toList());
            for (NamedNull namedNull : namedNullsOfFact)
            {
                graph.addVertex(namedNull);
                for (int i = namedNullsOfFact.indexOf(namedNull) + 1; i < namedNullsOfFact.size(); i++)
                {
                    graph.addVertex(namedNullsOfFact.get(i));
                    graph.addEdge(namedNull, namedNullsOfFact.get(i));
                }
            }
        }

        return graph;
    }

    protected DefaultUndirectedGraph<Term, DefaultEdge> calcRelatednessGraph(FindCoreKnowledgebase knowledgeBase)
    {
        DefaultUndirectedGraph<Term, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);

        for (Predicate predicate : knowledgeBase.getFactsPerPredicate().keySet())
        {
            ArrayList<Set<Term>> setsOfRelatedTerms = new ArrayList<>();
            for (int i = 0; i < predicate.getArity(); i++)
                setsOfRelatedTerms.add(new HashSet<>());

            for (PositiveLiteral literal : knowledgeBase.getFactsPerPredicate().get(predicate))
            {
                for (Term term : literal.getArguments())
                    setsOfRelatedTerms.get(literal.getArguments().indexOf(term)).add(term);
            }

            for (Set<Term> relatedTerms : setsOfRelatedTerms)
            {
                while (relatedTerms.size() > 0)
                {
                    Iterator<Term> it = relatedTerms.iterator();
                    Term currentTerm = it.next();
                    it.remove();

                    graph.addVertex(currentTerm);
                    while (it.hasNext()) {
                        Term term = it.next();
                        if (currentTerm != term && (currentTerm.getType().equals(TermType.NAMED_NULL) || term.getType().equals(TermType.NAMED_NULL))) {
                            //LOGGER.error("Edge set size: " + graph.edgeSet().size());
                            //LOGGER.error("Vertex set size: " + graph.vertexSet().size());
                            graph.addVertex(term);
                            graph.addEdge(currentTerm, term);
                        }
                    }
                }
            }
        }

        return graph;
    }

    public FindCoreKnowledgebase getCore()
    {
        return core;
    }

    public FindCoreKnowledgebase getT()
    {
        return knowledgeBase_T;
    }
    
    public FindCoreKnowledgebase getT_Sigma()
    {
        return knowledgeBase_T_Sigma;
    }
}
