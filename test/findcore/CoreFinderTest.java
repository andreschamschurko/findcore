package findcore;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Test;
import org.semanticweb.rulewerk.core.model.api.Fact;
import org.semanticweb.rulewerk.core.model.api.NamedNull;
import org.semanticweb.rulewerk.core.model.api.Term;
import org.semanticweb.rulewerk.core.reasoner.FindCoreKnowledgebase;
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase;
import org.semanticweb.rulewerk.parser.ParsingException;
import org.semanticweb.rulewerk.parser.RuleParser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class CoreFinderTest
{
    @Test
    public void calcT_XYTest()
    {
        CoreFinder findCoreSimpleExample = CoreFinder.findCore("./test-res/simpleExample/example_T.rls", "./test-res/simpleExample/example_T_Sigma.rls", false);

        NamedNull namedNull = null;
        Term term = null;

        assert findCoreSimpleExample != null;
        for (Fact fact : findCoreSimpleExample.getT_Sigma().getFacts())
        {
            if (fact.getPredicate().getName().equals("F") && fact.getArguments().get(0).getName().equals("c1"))
                namedNull = fact.getNamedNulls().collect(Collectors.toList()).get(0);
            else if (fact.getPredicate().getName().equals("F") && fact.getArguments().get(0).getName().equals("c4"))
                term = fact.getArguments().get(1);
        }

        FindCoreKnowledgebase knowledgeBase_T_XY = findCoreSimpleExample.calcT_XY(findCoreSimpleExample.getT_Sigma(), namedNull, term);
        assertEquals("The knowledge base has the wrong size", 11, knowledgeBase_T_XY.getFacts().size());
        assertEquals("The knowledge base has the wrong number of E facts", 5, knowledgeBase_T_XY.getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("E")).toArray().length);
        assertEquals("The knowledge base has the wrong number of F facts", 6, knowledgeBase_T_XY.getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("F")).toArray().length);

        CoreFinder findCoreExampleFromPaper = CoreFinder.findCore("./test-res/exampleFromPaper/example_T.rls", "./test-res/exampleFromPaper/example_T_Sigma.rls", false);

        assert findCoreExampleFromPaper != null;
        NamedNull namedNull_X1 = findCoreExampleFromPaper.getT_Sigma().getFacts().get(9).getNamedNulls().collect(Collectors.toList()).get(0);
        Term term_X2 = findCoreExampleFromPaper.getT_Sigma().getFacts().get(10).getArguments().get(1);

        NamedNull namedNull_X3 = findCoreExampleFromPaper.getT_Sigma().getFacts().get(11).getNamedNulls().collect(Collectors.toList()).get(0);
        Term term_X4 = findCoreExampleFromPaper.getT_Sigma().getFacts().get(12).getArguments().get(1);

        FindCoreKnowledgebase knowledgeBase_T_X1X2 = findCoreExampleFromPaper.calcT_XY(findCoreExampleFromPaper.getT_Sigma(), namedNull_X1, term_X2);

        FindCoreKnowledgebase knowledgeBase_U = new FindCoreKnowledgebase();
        knowledgeBase_U.addStatements(findCoreExampleFromPaper.getT_Sigma().getFacts().stream().filter(fact -> fact.getArguments().contains(namedNull_X1) || fact.getArguments().contains(term_X2)).collect(Collectors.toList()));
        FindCoreKnowledgebase knowledgeBase_T_X3X4 = findCoreExampleFromPaper.calcT_XY(knowledgeBase_U, namedNull_X3, term_X4);

        assertEquals("The knowledge base has the wrong size", 30, knowledgeBase_T_X1X2.getFacts().size());
        assertEquals("The knowledge base t_x1x2 has wrong elements", findCoreExampleFromPaper.getT_Sigma().getFacts(), knowledgeBase_T_X1X2.getFacts());

        assertEquals("The knowledge base has the wrong size", 18, knowledgeBase_T_X3X4.getFacts().size());

        Term term_Y = findCoreExampleFromPaper.getT_Sigma().getFacts().get(4).getArguments().get(0);

        FindCoreKnowledgebase knowledgeBase_T_X1Y = findCoreExampleFromPaper.calcT_XY(findCoreExampleFromPaper.getT_Sigma(), namedNull_X1, term_Y);
        assertEquals("The knowledge base has the wrong size", 30, knowledgeBase_T_X1Y.getFacts().size());
        assertEquals("The knowledge base has the wrong number of E facts", 13, knowledgeBase_T_X1Y.getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("E")).toArray().length);
        assertEquals("The knowledge base has the wrong number of F facts", 4, knowledgeBase_T_X1Y.getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("F")).toArray().length);

    }

    @Test
    public void calcGraphTest() throws IOException, ParsingException
    {
        CoreFinder findCore = CoreFinder.findCore("./test-res/simpleExample/example_T.rls", "./test-res/simpleExample/example_T_Sigma.rls", false);
        FindCoreKnowledgebase knowledgeBaseSimpleExample = new FindCoreKnowledgebase();
        knowledgeBaseSimpleExample.addStatements(RuleParser.parse(new FileInputStream("./test-res/simpleExample/example_T_Sigma.rls")).getFacts().stream().filter(fact -> !fact.getPredicate().getName().equalsIgnoreCase("Closure")).collect(Collectors.toList()));

        assert findCore != null;
        Graph<Term, DefaultEdge> graphSimpleExample = findCore.calcRelatednessGraph(knowledgeBaseSimpleExample);
        assertEquals("The vertex set of the graph has the wrong size", 14, graphSimpleExample.vertexSet().size());
        assertEquals("The edge set of the graph has the wrong size", 76, graphSimpleExample.edgeSet().size());


        FindCoreKnowledgebase knowledgeBasePaper = new FindCoreKnowledgebase();
        knowledgeBasePaper.addStatements(RuleParser.parse(new FileInputStream("./test-res/exampleFromPaper/example_T_Sigma.rls")).getFacts());

        Graph<Term, DefaultEdge> graphPaper = findCore.calcRelatednessGraph(knowledgeBasePaper);
        assertEquals("The vertex set of the graph has the wrong size", 9, graphPaper.vertexSet().size());
        assertEquals("The edge set of the graph has the wrong size", 26, graphPaper.edgeSet().size());
    }

    @Test
    public void calcGaifmanGraphTest() throws IOException, ParsingException
    {
        CoreFinder findCore = CoreFinder.findCore("./test-res/simpleExample/example_T.rls", "./test-res/simpleExample/example_T_Sigma.rls", false);
        FindCoreKnowledgebase knowledgeBaseSimpleExample = new FindCoreKnowledgebase();
        knowledgeBaseSimpleExample.addStatements(RuleParser.parse(new FileInputStream("./test-res/simpleExample/example_T_Sigma.rls")).getFacts().stream().filter(fact -> !fact.getPredicate().getName().equalsIgnoreCase("Closure")).collect(Collectors.toList()));

        assert findCore != null;
        Graph<NamedNull, DefaultEdge> graphSimpleExample = findCore.calcGaifmanGraph(knowledgeBaseSimpleExample);
        assertEquals("The vertex set of the graph has the wrong size", 8, graphSimpleExample.vertexSet().size());
        assertEquals("The edge set of the graph has the wrong size", 4, graphSimpleExample.edgeSet().size());

        findCore = CoreFinder.findCore("./test-res/exampleFromPaper/example_T.rls", "./test-res/exampleFromPaper/example_T_Sigma.rls", false);
        FindCoreKnowledgebase knowledgeBasePaper = new FindCoreKnowledgebase();
        knowledgeBasePaper.addStatements(RuleParser.parse(new FileInputStream("./test-res/exampleFromPaper/example_T_Sigma.rls")).getFacts());

        assert findCore != null;
        Graph<NamedNull, DefaultEdge> graphPaper = findCore.calcGaifmanGraph(knowledgeBasePaper);
        assertEquals("The vertex set of the graph has the wrong size", 4, graphPaper.vertexSet().size());
        assertEquals("The edge set of the graph has the wrong size", 6, graphPaper.edgeSet().size());
    }

    @Test
    public void findCoreTest() throws FileNotFoundException, ParsingException
    {
        KnowledgeBase knowledgeBaseSimpleExample = new KnowledgeBase();
        knowledgeBaseSimpleExample.addStatements(RuleParser.parse(new FileInputStream("./test-res/simpleExample/example_T.rls")).getFacts());
        knowledgeBaseSimpleExample.addStatements(RuleParser.parse(new FileInputStream("./test-res/simpleExample/rules.rls")).getStatements());
        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSimpleExample, false);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");

        start = System.nanoTime();
        CoreFinder coreFinderFromFiles = CoreFinder.findCore("./test-res/simpleExample/example_T.rls", "./test-res/simpleExample/example_T_Sigma.rls", false);
        finish = System.nanoTime();
        System.out.println("Simple example from file not random: " + (finish - start)/1000000 + " ms");

        assert coreFinderFromKnowledgeBase != null;
        assert coreFinderFromFiles != null;
        KnowledgeBase coreFromKnowledgeBase = coreFinderFromKnowledgeBase.getCore();
        KnowledgeBase coreFromFiles = coreFinderFromFiles.getCore();
        assertEquals("Wrong number of elements in core from knowledgeBase", 17, coreFromKnowledgeBase.getFacts().size());
        assertEquals("Wrong number of elements in core from file", 17, coreFromFiles.getFacts().size());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSimpleExample, true);
        finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase random: " + (finish - start)/1000000 + " ms");

        start = System.nanoTime();
        coreFinderFromFiles = CoreFinder.findCore("./test-res/simpleExample/example_T.rls", "./test-res/simpleExample/example_T_Sigma.rls", true);
        finish = System.nanoTime();
        System.out.println("Simple example from file random: " + (finish - start)/1000000 + " ms");

        assert coreFinderFromKnowledgeBase != null;
        assert coreFinderFromFiles != null;
        coreFromKnowledgeBase = coreFinderFromKnowledgeBase.getCore();
        coreFromFiles = coreFinderFromFiles.getCore();
        assertEquals("Wrong number of elements in core from knowledgeBase", 17, coreFromKnowledgeBase.getFacts().size());
        assertEquals("Wrong number of elements in core from file", 17, coreFromFiles.getFacts().size());

        KnowledgeBase knowledgeBaseExampleFromPaper = new KnowledgeBase();
        knowledgeBaseExampleFromPaper.addStatements(RuleParser.parse(new FileInputStream("./test-res/exampleFromPaper/example_T.rls")).getFacts());
        knowledgeBaseExampleFromPaper.addStatements(RuleParser.parse(new FileInputStream("./test-res/exampleFromPaper/rules.rls")).getStatements());
        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseExampleFromPaper, false);
        finish = System.nanoTime();
        System.out.println("Example from paper from knowledgebase not random: " + (finish - start)/1000000 + " ms");

        start = System.nanoTime();
        coreFinderFromFiles = CoreFinder.findCore("./test-res/exampleFromPaper/example_T.rls", "./test-res/exampleFromPaper/example_T_Sigma.rls", false);
        finish = System.nanoTime();
        System.out.println("Example from paper from file not random: " + (finish - start)/1000000 + " ms");

        assert coreFinderFromKnowledgeBase != null;
        assert coreFinderFromFiles != null;
        coreFromKnowledgeBase = coreFinderFromKnowledgeBase.getCore();
        coreFromFiles = coreFinderFromFiles.getCore();
        assertEquals("Wrong number of elements in core from knowledgeBase", 14, coreFromKnowledgeBase.getFacts().size());
        assertEquals("Wrong number of elements in core from file", 14, coreFromFiles.getFacts().size());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseExampleFromPaper, true);
        finish = System.nanoTime();
        System.out.println("Example from paper from knowledgebase random: " + (finish - start)/1000000 + " ms");

        start = System.nanoTime();
        coreFinderFromFiles = CoreFinder.findCore("./test-res/exampleFromPaper/example_T.rls", "./test-res/exampleFromPaper/example_T_Sigma.rls", true);
        finish = System.nanoTime();
        System.out.println("Example from paper from file random: " + (finish - start)/1000000 + " ms");

        assert coreFinderFromKnowledgeBase != null;
        assert coreFinderFromFiles != null;
        coreFromKnowledgeBase = coreFinderFromKnowledgeBase.getCore();
        coreFromFiles = coreFinderFromFiles.getCore();
        assertEquals("Wrong number of elements in core from knowledgeBase", 14, coreFromKnowledgeBase.getFacts().size());
        assertEquals("Wrong number of elements in core from file", 14, coreFromFiles.getFacts().size());
    }
}
