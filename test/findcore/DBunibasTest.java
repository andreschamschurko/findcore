package findcore;

import org.junit.Test;
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase;
import org.semanticweb.rulewerk.core.reasoner.Reasoner;
import org.semanticweb.rulewerk.parser.ParsingException;
import org.semanticweb.rulewerk.parser.RuleParser;
import org.semanticweb.rulewerk.reasoner.vlog.VLogReasoner;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class DBunibasTest
{
    private final String FOLDER_ROOT = "./test-res/dbunibas-chasebench/";

    @Test
    public void doctors10k_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "doctors/data/10k/";
        final String sources = "@source hospital[5] : load-csv(\"" + DATA_FOLDER + "hospital.csv\") ."
                + "@source medprescription[6] : load-csv(\"" + DATA_FOLDER + "medprescription.csv\") ."
                + "@source physician[4] : load-csv(\"" + DATA_FOLDER + "physician.csv\") ."
                + "@source treatment[5] : load-csv(\"" + DATA_FOLDER + "treatment.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "doctors/dependencies/doctors.st-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
        System.out.println("Number of physician facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")));
        System.out.println("Number of physician facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")));

        System.out.println("Number of prescription facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")));
        System.out.println("Number of prescription facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")));

        System.out.println("Number of doctor facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")));
        System.out.println("Number of doctor facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")));
    }

    @Test
    public void doctors_fd10k_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "doctors-fd/data/10k/";
        final String sources = "@source hospital[5] : load-csv(\"" + DATA_FOLDER + "hospital.csv\") ."
                + "@source medprescription[6] : load-csv(\"" + DATA_FOLDER + "medprescription.csv\") ."
                + "@source physician[4] : load-csv(\"" + DATA_FOLDER + "physician.csv\") ."
                + "@source treatment[5] : load-csv(\"" + DATA_FOLDER + "treatment.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "doctors-fd/dependencies/doctors-fd.st-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
        System.out.println("Number of physician facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")));
        System.out.println("Number of physician facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")));

        System.out.println("Number of prescription facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")));
        System.out.println("Number of prescription facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")));

        System.out.println("Number of doctor facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")));
        System.out.println("Number of doctor facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")));
    }

    @Test
    public void correctness_tgds_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER_CORRECTNESS_TGDS = FOLDER_ROOT + "correctness/tgds/data/";
        final String sources = "@source s[3] : load-csv(\"" + DATA_FOLDER_CORRECTNESS_TGDS + "s.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();

        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "correctness/tgds/dependencies/tgds.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "correctness/tgds/dependencies/tgds.t-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBaseRandom = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        long finish = System.nanoTime();
        System.out.println("Correctness.tgds from knowledgebase random: " + (finish - start)/1000000 + " ms");

        start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        finish = System.nanoTime();
        System.out.println("Correctness.tgds from knowledgebase not random: " + (finish - start)/1000000 + " ms");

        assert coreFinderFromKnowledgeBaseRandom.getCore().getFacts().size() == coreFinderFromKnowledgeBase.getCore().getFacts().size();

        System.out.println("Correctness.tgds number of inferences: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Correctness.tgds size of core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
        assertEquals("The vertex set of the graph has the wrong size", 6, coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void correctness_tgds5_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER_CORRECTNESS_TGDS5 = FOLDER_ROOT + "correctness/tgds5/data/";
        final String sources = "@source s0[4] : load-csv(\"" + DATA_FOLDER_CORRECTNESS_TGDS5 + "s0.csv\") ."
                + "@source s1[4] : load-csv(\"" + DATA_FOLDER_CORRECTNESS_TGDS5 + "s1.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();

        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "correctness/tgds5/dependencies/tgds5.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "correctness/tgds5/dependencies/tgds5.t-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBaseRandom = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        long finish = System.nanoTime();
        System.out.println("Correctness.tgds from knowledgebase random: " + (finish - start)/1000000 + " ms");

        start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        finish = System.nanoTime();
        System.out.println("Correctness.tgds from knowledgebase not random: " + (finish - start)/1000000 + " ms");

        assert coreFinderFromKnowledgeBaseRandom.getCore().getFacts().size() == coreFinderFromKnowledgeBase.getCore().getFacts().size();
        System.out.println("Correctness.tgds number of inferences: " + coreFinderFromKnowledgeBaseRandom.getT_Sigma().getFacts().size());
        System.out.println("Correctness.tgds size of core: " + coreFinderFromKnowledgeBaseRandom.getCore().getFacts().size());
        System.out.println("Correctness.tgds number of inferences: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Correctness.tgds size of core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void correctness_weak_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER_CORRECTNESS_WEAK = FOLDER_ROOT + "correctness/weak/data/";
        final String sources = "@source deptemp[3] : load-csv(\"" + DATA_FOLDER_CORRECTNESS_WEAK + "/deptemp.csv\").";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();

        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "correctness/weak/dependencies/weak.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "correctness/weak/dependencies/weak.t-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBaseRandom = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        long finish = System.nanoTime();
        System.out.println("Correctness.weak from knowledgebase random: " + (finish - start)/1000000 + " ms");

        start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        finish = System.nanoTime();
        System.out.println("Correctness.weak from knowledgebase not random: " + (finish - start)/1000000 + " ms");

        assert coreFinderFromKnowledgeBaseRandom.getCore().getFacts().size() == coreFinderFromKnowledgeBase.getCore().getFacts().size();
        System.out.println("Correctness.weak number of inferences: " + coreFinderFromKnowledgeBaseRandom.getT_Sigma().getFacts().size());
        System.out.println("Correctness.weak size of core: " + coreFinderFromKnowledgeBaseRandom.getCore().getFacts().size());
        System.out.println("Correctness.weak number of inferences: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Correctness.weak size of core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void deep100_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "deep/100/data/";
        StringBuilder sources = new StringBuilder();
        for (int i = 0; i <= 50; i++)
            sources.append("@source v").append(i).append("[4] : load-csv(\"").append(DATA_FOLDER).append("v").append(i).append(".csv\") .");

        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources.toString());

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "deep/100/dependencies/deep.st-tgds.rls")).getRules());

        System.out.println("Knowledgebase loaded ...");

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void doctors10_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "doctors/data/10/";
        final String sources = "@source hospital[5] : load-csv(\"" + DATA_FOLDER + "hospital.csv\") ."
                + "@source medprescription[6] : load-csv(\"" + DATA_FOLDER + "medprescription.csv\") ."
                + "@source physician[4] : load-csv(\"" + DATA_FOLDER + "physician.csv\") ."
                + "@source treatment[5] : load-csv(\"" + DATA_FOLDER + "treatment.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "doctors/dependencies/doctors.st-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void doctors100_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "doctors/data/100/";
        final String sources = "@source hospital[5] : load-csv(\"" + DATA_FOLDER + "hospital.csv\") ."
                + "@source medprescription[6] : load-csv(\"" + DATA_FOLDER + "medprescription.csv\") ."
                + "@source physician[4] : load-csv(\"" + DATA_FOLDER + "physician.csv\") ."
                + "@source treatment[5] : load-csv(\"" + DATA_FOLDER + "treatment.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "doctors/dependencies/doctors.st-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        System.out.println("Number of physician facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")).count());
        System.out.println("Number of physician facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")).count());

        System.out.println("Number of prescription facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")).count());
        System.out.println("Number of prescription facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")).count());

        System.out.println("Number of doctor facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")).count());
        System.out.println("Number of doctor facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")).count());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        System.out.println("Number of physician facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")).count());
        System.out.println("Number of physician facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("physician")).count());

        System.out.println("Number of prescription facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")).count());
        System.out.println("Number of prescription facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("prescription")).count());

        System.out.println("Number of doctor facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")).count());
        System.out.println("Number of doctor facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().stream().filter(fact -> fact.getPredicate().getName().equals("doctor")).count());
    }

    @Test
    public void doctors50_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "doctors/data/50/";
        final String sources = "@source hospital[5] : load-csv(\"" + DATA_FOLDER + "hospital.csv\") ."
                + "@source medprescription[6] : load-csv(\"" + DATA_FOLDER + "medprescription.csv\") ."
                + "@source physician[4] : load-csv(\"" + DATA_FOLDER + "physician.csv\") ."
                + "@source treatment[5] : load-csv(\"" + DATA_FOLDER + "treatment.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "doctors/dependencies/doctors.st-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void doctors250_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "doctors/data/250/";
        final String sources = "@source hospital[5] : load-csv(\"" + DATA_FOLDER + "hospital.csv\") ."
                + "@source medprescription[6] : load-csv(\"" + DATA_FOLDER + "medprescription.csv\") ."
                + "@source physician[4] : load-csv(\"" + DATA_FOLDER + "physician.csv\") ."
                + "@source treatment[5] : load-csv(\"" + DATA_FOLDER + "treatment.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "doctors/dependencies/doctors.st-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void doctors500_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "doctors/data/500/";
        final String sources = "@source hospital[5] : load-csv(\"" + DATA_FOLDER + "hospital.csv\") ."
                + "@source medprescription[6] : load-csv(\"" + DATA_FOLDER + "medprescription.csv\") ."
                + "@source physician[4] : load-csv(\"" + DATA_FOLDER + "physician.csv\") ."
                + "@source treatment[5] : load-csv(\"" + DATA_FOLDER + "treatment.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "doctors/dependencies/doctors.st-tgds.rls")).getRules());

        long start = System.nanoTime();
        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        long finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase not random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        start = System.nanoTime();
        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        finish = System.nanoTime();
        System.out.println("Simple example from knowledgebase random: " + (finish - start)/1000000 + " ms");
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void LUBM_Test() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "LUBM/data/001/";
        final String sources = "@source src_advisor[2] : load-csv(\"" + DATA_FOLDER + "src_advisor.csv\") ."
                + "@source src_AssistantProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssistantProfessor.csv\") ."
                + "@source src_AssociateProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssociateProfessor.csv\") ."
                + "@source src_Course[1] : load-csv(\"" + DATA_FOLDER + "src_Course.csv\") ."
                + "@source src_Department[1] : load-csv(\"" + DATA_FOLDER + "src_Department.csv\") ."
                + "@source src_doctoralDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_doctoralDegreeFrom.csv\") ."
                + "@source src_emailAddress[2] : load-csv(\"" + DATA_FOLDER + "src_emailAddress.csv\") ."
                + "@source src_FullProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_FullProfessor.csv\") ."
                + "@source src_GraduateCourse[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateCourse.csv\") ."
                + "@source src_GraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateStudent.csv\") ."
                + "@source src_headOf[2] : load-csv(\"" + DATA_FOLDER + "src_headOf.csv\") ."
                + "@source src_Lecturer[1] : load-csv(\"" + DATA_FOLDER + "src_Lecturer.csv\") ."
                + "@source src_mastersDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_mastersDegreeFrom.csv\") ."
                + "@source src_memberOf[2] : load-csv(\"" + DATA_FOLDER + "src_memberOf.csv\") ."
                + "@source src_name[2] : load-csv(\"" + DATA_FOLDER + "src_name.csv\") ."
                + "@source src_Publication[1] : load-csv(\"" + DATA_FOLDER + "src_Publication.csv\") ."
                + "@source src_publicationAuthor[2] : load-csv(\"" + DATA_FOLDER + "src_publicationAuthor.csv\") ."
                + "@source src_ResearchAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchAssistant.csv\") ."
                + "@source src_ResearchGroup[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchGroup.csv\") ."
                + "@source src_researchInterest[2] : load-csv(\"" + DATA_FOLDER + "src_researchInterest.csv\") ."
                + "@source src_subOrganizationOf[2] : load-csv(\"" + DATA_FOLDER + "src_subOrganizationOf.csv\") ."
                + "@source src_takesCourse[2] : load-csv(\"" + DATA_FOLDER + "src_takesCourse.csv\") ."
                + "@source src_teacherOf[2] : load-csv(\"" + DATA_FOLDER + "src_teacherOf.csv\") ."
                + "@source src_TeachingAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_TeachingAssistant.csv\") ."
                + "@source src_teachingAssistantOf[2] : load-csv(\"" + DATA_FOLDER + "src_teachingAssistantOf.csv\") ."
                + "@source src_telephone[2] : load-csv(\"" + DATA_FOLDER + "src_telephone.csv\") ."
                + "@source src_undergraduateDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_undergraduateDegreeFrom.csv\") ."
                + "@source src_UndergraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_UndergraduateStudent.csv\") ."
                + "@source src_University[1] : load-csv(\"" + DATA_FOLDER + "src_University.csv\") ."
                + "@source src_worksFor[2] : load-csv(\"" + DATA_FOLDER + "src_worksFor.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.t-tgds.rls")).getRules());

        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void LUBM_1000() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "LUBM/data/001-1000/";
        final String sources = "@source src_advisor[2] : load-csv(\"" + DATA_FOLDER + "src_advisor.csv\") ."
                + "@source src_AssistantProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssistantProfessor.csv\") ."
                + "@source src_AssociateProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssociateProfessor.csv\") ."
                + "@source src_Course[1] : load-csv(\"" + DATA_FOLDER + "src_Course.csv\") ."
                + "@source src_Department[1] : load-csv(\"" + DATA_FOLDER + "src_Department.csv\") ."
                + "@source src_doctoralDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_doctoralDegreeFrom.csv\") ."
                + "@source src_emailAddress[2] : load-csv(\"" + DATA_FOLDER + "src_emailAddress.csv\") ."
                + "@source src_FullProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_FullProfessor.csv\") ."
                + "@source src_GraduateCourse[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateCourse.csv\") ."
                + "@source src_GraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateStudent.csv\") ."
                + "@source src_headOf[2] : load-csv(\"" + DATA_FOLDER + "src_headOf.csv\") ."
                + "@source src_Lecturer[1] : load-csv(\"" + DATA_FOLDER + "src_Lecturer.csv\") ."
                + "@source src_mastersDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_mastersDegreeFrom.csv\") ."
                + "@source src_memberOf[2] : load-csv(\"" + DATA_FOLDER + "src_memberOf.csv\") ."
                + "@source src_name[2] : load-csv(\"" + DATA_FOLDER + "src_name.csv\") ."
                + "@source src_Publication[1] : load-csv(\"" + DATA_FOLDER + "src_Publication.csv\") ."
                + "@source src_publicationAuthor[2] : load-csv(\"" + DATA_FOLDER + "src_publicationAuthor.csv\") ."
                + "@source src_ResearchAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchAssistant.csv\") ."
                + "@source src_ResearchGroup[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchGroup.csv\") ."
                + "@source src_researchInterest[2] : load-csv(\"" + DATA_FOLDER + "src_researchInterest.csv\") ."
                + "@source src_subOrganizationOf[2] : load-csv(\"" + DATA_FOLDER + "src_subOrganizationOf.csv\") ."
                + "@source src_takesCourse[2] : load-csv(\"" + DATA_FOLDER + "src_takesCourse.csv\") ."
                + "@source src_teacherOf[2] : load-csv(\"" + DATA_FOLDER + "src_teacherOf.csv\") ."
                + "@source src_TeachingAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_TeachingAssistant.csv\") ."
                + "@source src_teachingAssistantOf[2] : load-csv(\"" + DATA_FOLDER + "src_teachingAssistantOf.csv\") ."
                + "@source src_telephone[2] : load-csv(\"" + DATA_FOLDER + "src_telephone.csv\") ."
                + "@source src_undergraduateDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_undergraduateDegreeFrom.csv\") ."
                + "@source src_UndergraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_UndergraduateStudent.csv\") ."
                + "@source src_University[1] : load-csv(\"" + DATA_FOLDER + "src_University.csv\") ."
                + "@source src_worksFor[2] : load-csv(\"" + DATA_FOLDER + "src_worksFor.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.t-tgds.rls")).getRules());

        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void LUBM_2500() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "LUBM/data/001-2500/";
        final String sources = "@source src_advisor[2] : load-csv(\"" + DATA_FOLDER + "src_advisor.csv\") ."
                + "@source src_AssistantProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssistantProfessor.csv\") ."
                + "@source src_AssociateProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssociateProfessor.csv\") ."
                + "@source src_Course[1] : load-csv(\"" + DATA_FOLDER + "src_Course.csv\") ."
                + "@source src_Department[1] : load-csv(\"" + DATA_FOLDER + "src_Department.csv\") ."
                + "@source src_doctoralDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_doctoralDegreeFrom.csv\") ."
                + "@source src_emailAddress[2] : load-csv(\"" + DATA_FOLDER + "src_emailAddress.csv\") ."
                + "@source src_FullProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_FullProfessor.csv\") ."
                + "@source src_GraduateCourse[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateCourse.csv\") ."
                + "@source src_GraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateStudent.csv\") ."
                + "@source src_headOf[2] : load-csv(\"" + DATA_FOLDER + "src_headOf.csv\") ."
                + "@source src_Lecturer[1] : load-csv(\"" + DATA_FOLDER + "src_Lecturer.csv\") ."
                + "@source src_mastersDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_mastersDegreeFrom.csv\") ."
                + "@source src_memberOf[2] : load-csv(\"" + DATA_FOLDER + "src_memberOf.csv\") ."
                + "@source src_name[2] : load-csv(\"" + DATA_FOLDER + "src_name.csv\") ."
                + "@source src_Publication[1] : load-csv(\"" + DATA_FOLDER + "src_Publication.csv\") ."
                + "@source src_publicationAuthor[2] : load-csv(\"" + DATA_FOLDER + "src_publicationAuthor.csv\") ."
                + "@source src_ResearchAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchAssistant.csv\") ."
                + "@source src_ResearchGroup[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchGroup.csv\") ."
                + "@source src_researchInterest[2] : load-csv(\"" + DATA_FOLDER + "src_researchInterest.csv\") ."
                + "@source src_subOrganizationOf[2] : load-csv(\"" + DATA_FOLDER + "src_subOrganizationOf.csv\") ."
                + "@source src_takesCourse[2] : load-csv(\"" + DATA_FOLDER + "src_takesCourse.csv\") ."
                + "@source src_teacherOf[2] : load-csv(\"" + DATA_FOLDER + "src_teacherOf.csv\") ."
                + "@source src_TeachingAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_TeachingAssistant.csv\") ."
                + "@source src_teachingAssistantOf[2] : load-csv(\"" + DATA_FOLDER + "src_teachingAssistantOf.csv\") ."
                + "@source src_telephone[2] : load-csv(\"" + DATA_FOLDER + "src_telephone.csv\") ."
                + "@source src_undergraduateDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_undergraduateDegreeFrom.csv\") ."
                + "@source src_UndergraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_UndergraduateStudent.csv\") ."
                + "@source src_University[1] : load-csv(\"" + DATA_FOLDER + "src_University.csv\") ."
                + "@source src_worksFor[2] : load-csv(\"" + DATA_FOLDER + "src_worksFor.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.t-tgds.rls")).getRules());

        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void LUBM_5000() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "LUBM/data/001-5000/";
        final String sources = "@source src_advisor[2] : load-csv(\"" + DATA_FOLDER + "src_advisor.csv\") ."
                + "@source src_AssistantProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssistantProfessor.csv\") ."
                + "@source src_AssociateProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssociateProfessor.csv\") ."
                + "@source src_Course[1] : load-csv(\"" + DATA_FOLDER + "src_Course.csv\") ."
                + "@source src_Department[1] : load-csv(\"" + DATA_FOLDER + "src_Department.csv\") ."
                + "@source src_doctoralDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_doctoralDegreeFrom.csv\") ."
                + "@source src_emailAddress[2] : load-csv(\"" + DATA_FOLDER + "src_emailAddress.csv\") ."
                + "@source src_FullProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_FullProfessor.csv\") ."
                + "@source src_GraduateCourse[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateCourse.csv\") ."
                + "@source src_GraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateStudent.csv\") ."
                + "@source src_headOf[2] : load-csv(\"" + DATA_FOLDER + "src_headOf.csv\") ."
                + "@source src_Lecturer[1] : load-csv(\"" + DATA_FOLDER + "src_Lecturer.csv\") ."
                + "@source src_mastersDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_mastersDegreeFrom.csv\") ."
                + "@source src_memberOf[2] : load-csv(\"" + DATA_FOLDER + "src_memberOf.csv\") ."
                + "@source src_name[2] : load-csv(\"" + DATA_FOLDER + "src_name.csv\") ."
                + "@source src_Publication[1] : load-csv(\"" + DATA_FOLDER + "src_Publication.csv\") ."
                + "@source src_publicationAuthor[2] : load-csv(\"" + DATA_FOLDER + "src_publicationAuthor.csv\") ."
                + "@source src_ResearchAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchAssistant.csv\") ."
                + "@source src_ResearchGroup[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchGroup.csv\") ."
                + "@source src_researchInterest[2] : load-csv(\"" + DATA_FOLDER + "src_researchInterest.csv\") ."
                + "@source src_subOrganizationOf[2] : load-csv(\"" + DATA_FOLDER + "src_subOrganizationOf.csv\") ."
                + "@source src_takesCourse[2] : load-csv(\"" + DATA_FOLDER + "src_takesCourse.csv\") ."
                + "@source src_teacherOf[2] : load-csv(\"" + DATA_FOLDER + "src_teacherOf.csv\") ."
                + "@source src_TeachingAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_TeachingAssistant.csv\") ."
                + "@source src_teachingAssistantOf[2] : load-csv(\"" + DATA_FOLDER + "src_teachingAssistantOf.csv\") ."
                + "@source src_telephone[2] : load-csv(\"" + DATA_FOLDER + "src_telephone.csv\") ."
                + "@source src_undergraduateDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_undergraduateDegreeFrom.csv\") ."
                + "@source src_UndergraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_UndergraduateStudent.csv\") ."
                + "@source src_University[1] : load-csv(\"" + DATA_FOLDER + "src_University.csv\") ."
                + "@source src_worksFor[2] : load-csv(\"" + DATA_FOLDER + "src_worksFor.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.t-tgds.rls")).getRules());

        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());

        coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, true);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void LUBM_7500() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "LUBM/data/001-7500/";
        final String sources = "@source src_advisor[2] : load-csv(\"" + DATA_FOLDER + "src_advisor.csv\") ."
                + "@source src_AssistantProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssistantProfessor.csv\") ."
                + "@source src_AssociateProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssociateProfessor.csv\") ."
                + "@source src_Course[1] : load-csv(\"" + DATA_FOLDER + "src_Course.csv\") ."
                + "@source src_Department[1] : load-csv(\"" + DATA_FOLDER + "src_Department.csv\") ."
                + "@source src_doctoralDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_doctoralDegreeFrom.csv\") ."
                + "@source src_emailAddress[2] : load-csv(\"" + DATA_FOLDER + "src_emailAddress.csv\") ."
                + "@source src_FullProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_FullProfessor.csv\") ."
                + "@source src_GraduateCourse[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateCourse.csv\") ."
                + "@source src_GraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateStudent.csv\") ."
                + "@source src_headOf[2] : load-csv(\"" + DATA_FOLDER + "src_headOf.csv\") ."
                + "@source src_Lecturer[1] : load-csv(\"" + DATA_FOLDER + "src_Lecturer.csv\") ."
                + "@source src_mastersDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_mastersDegreeFrom.csv\") ."
                + "@source src_memberOf[2] : load-csv(\"" + DATA_FOLDER + "src_memberOf.csv\") ."
                + "@source src_name[2] : load-csv(\"" + DATA_FOLDER + "src_name.csv\") ."
                + "@source src_Publication[1] : load-csv(\"" + DATA_FOLDER + "src_Publication.csv\") ."
                + "@source src_publicationAuthor[2] : load-csv(\"" + DATA_FOLDER + "src_publicationAuthor.csv\") ."
                + "@source src_ResearchAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchAssistant.csv\") ."
                + "@source src_ResearchGroup[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchGroup.csv\") ."
                + "@source src_researchInterest[2] : load-csv(\"" + DATA_FOLDER + "src_researchInterest.csv\") ."
                + "@source src_subOrganizationOf[2] : load-csv(\"" + DATA_FOLDER + "src_subOrganizationOf.csv\") ."
                + "@source src_takesCourse[2] : load-csv(\"" + DATA_FOLDER + "src_takesCourse.csv\") ."
                + "@source src_teacherOf[2] : load-csv(\"" + DATA_FOLDER + "src_teacherOf.csv\") ."
                + "@source src_TeachingAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_TeachingAssistant.csv\") ."
                + "@source src_teachingAssistantOf[2] : load-csv(\"" + DATA_FOLDER + "src_teachingAssistantOf.csv\") ."
                + "@source src_telephone[2] : load-csv(\"" + DATA_FOLDER + "src_telephone.csv\") ."
                + "@source src_undergraduateDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_undergraduateDegreeFrom.csv\") ."
                + "@source src_UndergraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_UndergraduateStudent.csv\") ."
                + "@source src_University[1] : load-csv(\"" + DATA_FOLDER + "src_University.csv\") ."
                + "@source src_worksFor[2] : load-csv(\"" + DATA_FOLDER + "src_worksFor.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.t-tgds.rls")).getRules());

        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void LUBM_10000() throws IOException, ParsingException
    {
        String DATA_FOLDER = FOLDER_ROOT + "LUBM/data/001-10000/";
        final String sources = "@source src_advisor[2] : load-csv(\"" + DATA_FOLDER + "src_advisor.csv\") ."
                + "@source src_AssistantProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssistantProfessor.csv\") ."
                + "@source src_AssociateProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_AssociateProfessor.csv\") ."
                + "@source src_Course[1] : load-csv(\"" + DATA_FOLDER + "src_Course.csv\") ."
                + "@source src_Department[1] : load-csv(\"" + DATA_FOLDER + "src_Department.csv\") ."
                + "@source src_doctoralDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_doctoralDegreeFrom.csv\") ."
                + "@source src_emailAddress[2] : load-csv(\"" + DATA_FOLDER + "src_emailAddress.csv\") ."
                + "@source src_FullProfessor[1] : load-csv(\"" + DATA_FOLDER + "src_FullProfessor.csv\") ."
                + "@source src_GraduateCourse[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateCourse.csv\") ."
                + "@source src_GraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_GraduateStudent.csv\") ."
                + "@source src_headOf[2] : load-csv(\"" + DATA_FOLDER + "src_headOf.csv\") ."
                + "@source src_Lecturer[1] : load-csv(\"" + DATA_FOLDER + "src_Lecturer.csv\") ."
                + "@source src_mastersDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_mastersDegreeFrom.csv\") ."
                + "@source src_memberOf[2] : load-csv(\"" + DATA_FOLDER + "src_memberOf.csv\") ."
                + "@source src_name[2] : load-csv(\"" + DATA_FOLDER + "src_name.csv\") ."
                + "@source src_Publication[1] : load-csv(\"" + DATA_FOLDER + "src_Publication.csv\") ."
                + "@source src_publicationAuthor[2] : load-csv(\"" + DATA_FOLDER + "src_publicationAuthor.csv\") ."
                + "@source src_ResearchAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchAssistant.csv\") ."
                + "@source src_ResearchGroup[1] : load-csv(\"" + DATA_FOLDER + "src_ResearchGroup.csv\") ."
                + "@source src_researchInterest[2] : load-csv(\"" + DATA_FOLDER + "src_researchInterest.csv\") ."
                + "@source src_subOrganizationOf[2] : load-csv(\"" + DATA_FOLDER + "src_subOrganizationOf.csv\") ."
                + "@source src_takesCourse[2] : load-csv(\"" + DATA_FOLDER + "src_takesCourse.csv\") ."
                + "@source src_teacherOf[2] : load-csv(\"" + DATA_FOLDER + "src_teacherOf.csv\") ."
                + "@source src_TeachingAssistant[1] : load-csv(\"" + DATA_FOLDER + "src_TeachingAssistant.csv\") ."
                + "@source src_teachingAssistantOf[2] : load-csv(\"" + DATA_FOLDER + "src_teachingAssistantOf.csv\") ."
                + "@source src_telephone[2] : load-csv(\"" + DATA_FOLDER + "src_telephone.csv\") ."
                + "@source src_undergraduateDegreeFrom[2] : load-csv(\"" + DATA_FOLDER + "src_undergraduateDegreeFrom.csv\") ."
                + "@source src_UndergraduateStudent[1] : load-csv(\"" + DATA_FOLDER + "src_UndergraduateStudent.csv\") ."
                + "@source src_University[1] : load-csv(\"" + DATA_FOLDER + "src_University.csv\") ."
                + "@source src_worksFor[2] : load-csv(\"" + DATA_FOLDER + "src_worksFor.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();
        KnowledgeBase knowledgeBaseSourcesLoaded = new KnowledgeBase();
        knowledgeBaseSourcesLoaded.addStatements(reasoner.getInferences().collect(Collectors.toList()));
        reasoner.close();

        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.st-tgds.rls")).getRules());
        knowledgeBaseSourcesLoaded.addStatements(RuleParser.parse(new FileInputStream(FOLDER_ROOT + "LUBM/dependencies/LUBM.t-tgds.rls")).getRules());

        CoreFinder coreFinderFromKnowledgeBase = CoreFinder.findCore(knowledgeBaseSourcesLoaded, false);
        assert coreFinderFromKnowledgeBase != null;
        System.out.println("Number of facts in T_Sigma: " + coreFinderFromKnowledgeBase.getT_Sigma().getFacts().size());
        System.out.println("Number of facts in core: " + coreFinderFromKnowledgeBase.getCore().getFacts().size());
    }

    @Test
    public void letItLeak() throws IOException, ParsingException
    {
        String DATA_FOLDER_CORRECTNESS_TGDS = FOLDER_ROOT + "correctness/tgds/data/";
        final String sources = "@source s[3] : load-csv(\"" + DATA_FOLDER_CORRECTNESS_TGDS + "s.csv\") .";
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        RuleParser.parseInto(knowledgeBase, sources);

        Reasoner reasoner = new VLogReasoner(knowledgeBase);
        reasoner.reason();

        for (int i=0; i < 10000000; i++)
            reasoner.getInferences();

        reasoner.close();
    }

}
