# findCore
A java implementation of the FINDCORE algorithm from the paper "Efficient core computation in data exchange" of Georg Gottlob and Alan Nash (https://dl.acm.org/doi/10.1145/1346330.1346334). It determines cores of rulewerk programs (https://github.com/knowsys/rulewerk/wiki).

## Dependencies
- junit
- rulewerk-core
- rulewerk-parser
- rulewerk-vlog
- jgrapht-core
- jgrapht-unimi-dsi
- slf4j-api
- slf4j-simple

## Strategies
- Picking a random y to an x
- Picking y to an x using a relatedness graph to preselect the possible y

## Used Examples in the tests
- small own example with 6 constant names and one rule.
- the small example from the FINDCORE paper
- dbunibas-chasebench
    - correctness
    - deep
        - 100
    - doctors
        - 10
        - 50
        - 100
        - 250
        - 500
        - 10k
    - doctors-df
        - 10k
    - LUBM
